var _ = require('lodash');
var logger = require('./logging').module('title');

module.exports = function (addon) {
    var hipchat = require('./hipchat')(addon);
    var repositories = require('../model/repositories')(addon);
    var scheduledUpdates = {};


    return {
        scheduleUpdate: function(clientInfo) {
            if (! scheduledUpdates[clientInfo.clientKey]) {
                var self = this;
                scheduledUpdates[clientInfo.clientKey] = clientInfo;

                setTimeout(function() {
                    self.updateTitle(clientInfo);
                    scheduledUpdates = _.omit(scheduledUpdates, clientInfo.clientKey);
                }, 5000);
            }
        },

        updateTitle: function (clientInfo) {
            return hipchat.getRoom(clientInfo)
                    .then(function (result) {
                        logger.info("Updating HC title");

                        return repositories.lockedRepositories(clientInfo).then(function (repositories) {
                            var title = _.chain(repositories)
                                    .sortBy(function(r) {return r.title;})
                                    .groupBy(function(r) {return r.lockedByName;})
                                    .pairs()
                                    .map(function(repos) {
                                        return _.map(repos[1], function(r) {return r.title;}).join(', ') + " (" + repos[0] + ")";
                                    }).join(", ");

                            if (title == "") {
                                title = "Nothing";
                            }

                            var existingTopic = result.body.topic;
                            var positionOfSeparator = existingTopic.indexOf("#");

                            if (positionOfSeparator == -1) {
                                var topic = "Locked: " + title + " # " + existingTopic;
                            } else {
                                topic = "Locked: " + title + " # " + existingTopic.substr(positionOfSeparator + 2);
                            }

                            if (topic != existingTopic) {
                                return hipchat.setTitle(clientInfo, topic)
                                        .except(function(ex) {
                                            logger.warn(ex);
                                        });
                            }
                        });
                    });
        }
    };
};
