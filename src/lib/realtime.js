var _ = require('lodash');
var clients = {};

module.exports = function (app,addon) {

    var users = require('../model/users');

    // Auth
    app.io.use(function (socket, next) {
        if (! socket.request.session.clientKey) {
            return next(new Error("Unauthorized"));
        }

        socket.clientKey = socket.request.session.clientKey;

        next();
    });

    var addClient = function (client) {
        var clientKey = client.clientKey;

        if (!clients[clientKey]) {
            clients[clientKey] = [];
        }

        clients[clientKey].push(client);
    };

    var removeClient = function (client) {
        var contextId = client.clientKey;
        _.remove(clients[contextId], client);

        if (clients[contextId].length == 0) {
            clients = _.omit(clients, contextId);
        }
    };

    app.io.on('connection', function (socket) {
        console.log('Client connected');
        addClient(socket);

        socket.on('disconnect', function () {
            console.log('Client disconnected');
            removeClient(socket);
        })
    });

    return {
        emit: function (clientInfo, event, data) {
            var recipients = clients[clientInfo.clientKey];

            if (!recipients) return;

            console.log('Emitting event', event, 'to', clientInfo.clientKey, 'for', recipients.length, 'users');

            _.each(recipients, function (client) {
                client.emit(event, data);
            })
        }
    }
};
