var _ = require('lodash');

var UserError = function(message) {
    this.message = message;
};

UserError.prototype = _.create(Error.prototype, {
    'constructor': UserError
});

module.exports = {
    UserError: UserError
};
