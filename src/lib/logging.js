var bunyan = require('bunyan');
var _ = require("lodash");

var logger = require('bunyan').createLogger({
    name: 'repo-locker',
    level: 'debug',
    serializers: bunyan.stdSerializers
});

logger.module = function(name, attributes) {
  return this.child(_.merge({moduleName: name}, attributes));
};

module.exports = logger;