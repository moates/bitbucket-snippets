var _ = require('lodash');
var logger = require('./logging').module('glances');

module.exports = function (addon) {
    var hipchat = require('./hipchat')(addon);
    var repositories = require('../model/repositories')(addon);
    var scheduledUpdates = {};

    return {
        renderGlance: function(clientInfo) {
            return repositories.lockedRepositories(clientInfo)
                    .then(function (results) {

                        var global = _.filter(results, function(result) {return result.global;});

                        var count = results.length;
                        var type = "success";
                        var locked = "Repo Locker";

                        if (_.any(global)) {
                            locked = _.map(global, function (r) {return r.title}).join(', ');
                            type = "error";
                        } else if (results.length > 0) {
                            locked = _.map(results, function (r) {return r.title}).join(', ');
                            type = "current";
                        }

                        return {
                            label: {
                                type: "html",
                                value: locked
                            },
                            status: {
                                type: "lozenge",
                                value: {
                                    label: String(count),
                                    type: type
                                }
                            }
                        };
                    });
        },

        updateGlances: function (clientInfo) {
            return this.renderGlance(clientInfo).then(function(data) {
                return hipchat.pushGlance(clientInfo,'glance.repolocker',data);
            }).catch(function(ex) {
                logger.warn(ex);
            });
        }
    };
};