var Promise = require('bluebird');
var _ = require("lodash");
var logging = require("../lib/logging").module("repositories");
var errors = require("../lib/errors");
var sequelize = require('sequelize');

module.exports = function (addon) {
    var Alias = addon.settings.alias;
    var logExceptions = function (ex) {
        logging.warn(ex);
        return Promise.rejected(ex);
    };

    /**
     * @param {{groupId:Integer,roomId:Integer}} clientInfo
     * @returns {Repository}
     */
    var aliasByUser = function (clientInfo, userId) {
        return Alias.scope({
            method: ["byUser", clientInfo.groupId, userId]
        });
    };

    return {
        all: function (clientInfo, userId) {			
            return aliasByUser(clientInfo, userId).findAll().then(function (success) {
                return _.map(success, function (result) {return result.alias;});
            });
        },

        ensureAlias: function (clientInfo, userId, alias) {
            return Alias.findOrCreate({ where: {
                groupId: clientInfo.groupId,
                userId: userId,
                alias: alias
            }})
        }
    };
};