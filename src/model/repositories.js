var Promise = require('bluebird');
var _ = require("lodash");
var logging = require("../lib/logging").module("repositories");
var errors = require("../lib/errors");
var sequelize = require('sequelize');

module.exports = function (addon) {
    var Repository = addon.settings.repository;
    var logExceptions = function (ex) {
        logging.warn(ex);
        return Promise.rejected(ex);
    };


    /**
     * @param {{groupId:Integer,roomId:Integer}} clientInfo
     * @returns {Repository}
     */
    var repositoriesByGroup = function (clientInfo) {
        return Repository.scope({
            method: ["byClient", clientInfo.groupId, clientInfo.roomId]
        });
    };

    return {
        /**
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {string} title
         * @param {string} link
         * @param {boolean} global
         * @returns Promise.<Repository>
         */
        addRepository: function (clientInfo, title, link, global) {
            return Repository.create({
                title: title,
                lowertitle: title.toLowerCase(),
                groupId: clientInfo.groupId,
                roomId: clientInfo.roomId,
                link: link,
                locked: false,
                global: global
            });
        },

        /**
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {string} id
         * @returns Promise.<Repository>
         */
        updateRepository: function (clientInfo, data) {
            data.lowertitle = data.title.toLowerCase();
            return repositoriesByGroup(clientInfo).update(data, {
                where: { id: data.id },
                returning: true
            });
        },

        /**
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {string} id
         * @returns Promise.<Repository>
         */
        deleteRepository: function (clientInfo, id) {
            return repositoriesByGroup(clientInfo).destroy({
                where: { id: id }
            });
        },

        /**
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @returns {Promise.<Array.<Repository>>}
         */
        allRepositories: function (clientInfo) {
            return repositoriesByGroup(clientInfo)
                    .findAll({
                        order: ['global']
                    })
                    .catch(logExceptions);
        },

        /**
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @returns {Promise.<Array.<Repository>>}
         */
        lockedRepositories: function (clientInfo) {
            return repositoriesByGroup(clientInfo)
                    .findAll({
                        where: {
                            locked: true
                        },
                        order: ['id']
                    })
                    .catch(logExceptions);
        },

        /**
         *
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @returns {Promise.<Number>}
         */
        lockedRepositoriesCount: function (clientInfo) {
            return repositoriesByGroup(clientInfo)
                    .count({
                        where: {
                            locked: true
                        }
                    })
                    .catch(logExceptions);
        },

        /**
         *
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {{id:Number,name:String}} user
         * @param {Number} id
         * @returns {Promise.<Repository>}
         */
        lockRepository: function (clientInfo, user, id) {
            return repositoriesByGroup(clientInfo)
                    .update({
                        locked: true,
                        lockedBy: user.id,
                        lockedByName: user.mention_name
                    },
                    {
                        where: {
                            id: id,
                            locked: false
                        },
                        returning: true
                    }).spread(function (count, results) {
                        return results;
                    }).then(function (results) {
                        return _.first(results);
                    })
                    .catch(logExceptions);
        },

        /**
         *
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {Number} id
         * @returns {Promise.<Repository>}
         */
        unlockRepository: function (clientInfo, id) {
            return repositoriesByGroup(clientInfo)
                    .update({
                        locked: false,
                        lockedBy: 0,
                        lockedByName: ''
                    },
                    {
                        where: {
                            id: id,
                            locked: true
                        },
                        returning: true
                    }).spread(function (count, results) {
                        return results;
                    }).then(function (results) {
                        return _.first(results);
                    })
                    .catch(logExceptions);
        },

        /**
         *
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {{id: Number, name: String}} user
         * @param {Array.<String>} names
         * @returns {Promise.<Array.<Number>>}
         */
        lockAllRepositories: function (clientInfo, user, names) {
            var lowertitles = _.map(names, function (n) {return n.toLowerCase();});
            return repositoriesByGroup(clientInfo).findAll({
                where: {
                    $or: [{
                        lowertitle: {
                            $in: lowertitles
                        }
                    }, {
                        global: true,
                        locked: true
                    }]
                }
            }).then(function (results) {
                if (_.any(results, function(result) { return result.global && result.locked; })) {
                    var lockedGlobals = _.chain(results)
                            .filter(function(result) {return result.global;})
                            .map(function(result) {return result.title;})
                            .values();

                    var error = new errors.UserError("Can't lock repositories, as there is a global lock:  " + lockedGlobals.join(', '));
                    logging.info(error);
                    return Promise.reject(error);
                }

                var differences = _.difference(lowertitles, _.map(results, function (r) { return r.lowertitle; }));
                var alreadyLocked = _.any(_.map(results, function (r) {return r.locked;}));

                if (differences.length > 0) {
                    error = new errors.UserError("Could not find the following repositories " + differences.join(', '));
                    logging.info(error);
                    return Promise.reject(error);
                } else if (alreadyLocked) {
                    var lockedRepositories = _.filter(results, function (r) {return r.locked});
                    var titles = _.map(lockedRepositories, function (r) {return r.title});

                    error = new errors.UserError("Some repositories were already locked: " + titles.join(", "));
                    logging.info(error);
                    return Promise.reject(error);
                } else {
                    return repositoriesByGroup(clientInfo).update({
                        locked: true,
                        lockedBy: user.id,
                        lockedByName: '@' + user.mention_name
                    }, {
                        where: {
                            lowertitle: {
                                $in: lowertitles
                            }
                        },
                        returning: true
                    }).spread(function (count, results) {
                        return results;
                    });
                }
            }).catch(logExceptions);
        },

        /**
         *
         * @param {{groupId:Integer,roomId:Integer}} clientInfo
         * @param {String} user
         * @param {Array.<String>} names
         * @returns {Promise.<Array.<Number>>}
         */
        unlockAllRepositories: function (clientInfo, user, names) {
            var lowertitles = _.map(names, function (n) {return n.toLowerCase();});
            return repositoriesByGroup(clientInfo).findAll({
                where: {
                    lowertitle: {
                        $in: lowertitles
                    }
                }
            }).then(function (results) {
                var differences = _.difference(lowertitles, _.map(results, function (r) { return r.lowertitle; }));

                var alreadyLocked = _.all(_.map(results, function (r) {return r.locked;}));

                if (differences.length > 0) {
                    var error = new errors.UserError("Could not find the following repositories " + differences.join(', '));
                    logging.info(error);
                    return Promise.rejected(error);
                } else if (!alreadyLocked) {
                    var lockedRepositories = _.filter(results, function (r) {return ! r.locked});
                    var titles = _.map(lockedRepositories, function (r) {return r.title});

                    error = new errors.UserError("Some repositories were already unlocked: " + titles.join(", "));
                    logging.info(error);
                    return Promise.rejected(error);
                } else {
                    return repositoriesByGroup(clientInfo).update({
                        locked: false,
                        lockedBy: 0,
                        lockedByName: ''
                    }, {
                        where: {
                            lowertitle: {
                                $in: lowertitles
                            }
                        },
                        returning: true
                    }).spread(function (count, results) {
                        return results;
                    });
                }
            }).catch(logExceptions);
        }
    }
};
