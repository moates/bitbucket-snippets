var Promise = require('rsvp');
var _ = require('lodash');

module.exports = function(addon) {

  var hipchatClient = require('../lib/hipchat')(addon);

  return {
    findById: function (clientInfo, userId) {
        var findUser = function (p) {return p.id == userId};

        return hipchatClient.getRoom(clientInfo, userId)
                .then(function(response) {
                    return _.find(response.body.participants, findUser)});
    }
  }
};
