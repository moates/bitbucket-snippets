var sequelize = require('sequelize');

module.exports =  function(schema) {
    var repository = schema.define('repository', {
        title: {
            type: sequelize.STRING,
            validate: {
                notEmpty: true
            }
        },
        lowertitle: {
            type: sequelize.STRING,
            unique: 'repository_unique_identifier',
            allowNull: false,
            validate: {
                isLowercase: true
            }
        },
        groupId: {
            type: sequelize.INTEGER,
            unique: 'repository_unique_identifier',
            allowNull: false
        },
        roomId: {
            type: sequelize.INTEGER,
            unique: 'repository_unique_identifier',
            allowNull: false
        },
        link: {
            type: sequelize.STRING,
            length: 2048,
            validate: {
                isUrl: true
            }
        },
        locked: {
            type: sequelize.BOOLEAN,
            default: false,
            allowNull: false
        },
        lockedBy: {
            type: sequelize.INTEGER
        },
        lockedByName: {
            type: sequelize.STRING
        },
        global: {
            type: sequelize.BOOLEAN,
            allowNull: false,
            default: false
        }
    }, {
        indexes: [{
            name: "idx_repository_title",
            fields: ['title']
        }, {
            name: "idx_repository_group_room",
            fields: ['groupId', 'roomId']
        }],
        scopes: {
            byClient: function (group, room) {
                return {
                    where: {
                        groupId: group,
                        roomId: room
                    }
                }
            }
        }
    });

    repository.sync();

    return repository;
};
