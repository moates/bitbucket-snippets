var sequelize = require('sequelize');

module.exports = function (schema) {
    var client = schema.define('client', {
        key: {
            type: sequelize.STRING
        },
        clientKey: {
            type: sequelize.STRING
        },
        val: {
            type: sequelize.JSON
        }
    }, {
        indexes: [{
            name: "idx_client_key_key",
            fields: ['clientKey', 'key']
        }]
    });

    client.sync();

    return client;
};
