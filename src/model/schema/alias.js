var sequelize = require('sequelize');

module.exports = function (schema) {
    var alias = schema.define('alias', {
        groupId: {
            type: sequelize.INTEGER,
            unique: 'unique_alias',
            allowNull: false
        },
        userId: {
            type: sequelize.INTEGER,
            unique: 'unique_alias',
            allowNull: false
        },
        alias: {
            type: sequelize.STRING,
            unique: 'unique_alias',
            allowNull: false
        }
    }, {
        indexes: [{
            name: "idx_user_group",
            fields: ['groupId', 'userId']
        }],
        scopes: {
            byUser: function (group, user) {
                return {
                    where: {
                        groupId: group,
                        userId: user
                    }
                }
            }
        }
    });

    alias.sync();

    return alias;
};
