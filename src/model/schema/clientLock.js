var sequelize = require('sequelize');

module.exports = function (schema) {
    var clientLock = schema.define('clientLock', {
        groupId: {
            type: sequelize.INTEGER,
            unique: 'client_lock_unique_identifier',
            validate: {
                notNull: true
            }
        },
        roomId: {
            type: sequelize.INTEGER,
            unique: 'client_lock_unique_identifier',
            validate: {
                notNull: true
            }
        },
        locked: {
            type: sequelize.STRING
        },
        lockedBy: {
            type: sequelize.INTEGER
        },
        lockedByName: {
            type: sequelize.STRING
        }

    }, {
        indexes: [{
            name: "idx_clientlock_group_room",
            fields: ['groupId', 'roomId']
        }],
        scopes: {
            byClient: function (group, roomId) {
                return {
                    where: {
                        groupId: group,
                        roomId: roomId
                    }
                }
            }
        }
    });

    clientLock.sync();

    return clientLock;
};
