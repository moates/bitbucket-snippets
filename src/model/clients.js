var url = require('url');
var RSVP = require('rsvp');
var logger = require('../lib/logging');
var Sequelize = require('sequelize');

function SequelizeDB(_, opts) {
    this.schema = new Sequelize(opts.url, {logging: false});

    this.client = require('./schema/client')(this.schema);
    this.clientLock = require('./schema/clientLock')(this.schema);
    this.repository = require('./schema/repository')(this.schema);
    this.alias = require('./schema/alias')(this.schema);
}

var proto = SequelizeDB.prototype;

proto.get = function (key, clientKey) {
    return this.client.findOne({
        where: {
            clientKey: clientKey,
            key: key
        }
    }).then(function(data) {
        if (data)
            return data.val;
        return data;
    }).catch(function(ex) {
        logger.warn(ex);
        return ex;
    });
};

proto.set = function (key, val, clientKey) {
    var self = this;

    return this.client.insertOrUpdate({
        clientKey: clientKey,
        key: key,
        val: val
    }).catch(function(ex) {
        logger.warn(ex);
        return ex;
    }).then(function() {
        return self.get(key,clientKey);
    })
};

proto.del = function (key, clientKey) {
    return this.client.destroy({
        clientKey: clientKey,
        key: key
    }).catch(function(ex) {
        logger.warn(ex);
    })
};

module.exports = function (logger, opts) {
    return new SequelizeDB(logger, opts);
};