module.exports = function (app, addon) {
  app.get('/debug/healthcheck', function (req, res) {
    res.send('OK');
  });
};
