var _ = require('lodash');
var logger = require('../lib/logging').module('webhook');
var errors = require('../lib/errors');
var Promise = require('bluebird');

module.exports = function (app, addon) {

    var repositories = addon.repositories;
    var users = require('../model/users')(addon);
    var hipchat = require('../lib/hipchat')(addon);
    var title = require('../lib/title')(addon);
    var glances = require('../lib/glances')(addon);

    var commands = {};

    function modifyLock(clientInfo, method, from, eventName, names) {
        return method(clientInfo, from, names)
                .then(function (repositories) {
                    app.realtime.emit(clientInfo, eventName, repositories);
                    title.updateTitle(clientInfo);
                    glances.updateGlances(clientInfo);
                }).catch(errors.UserError, function (err) {
                    console.info(err, "Sending message back to user");

                    hipchat.sendMessage(clientInfo, clientInfo.roomId, err.message, {"color": "red"});
                });
    }

    function render(res, template, options) {
        return new Promise(function (resolve, reject) {
            res.render(template, options, function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        });
    }

    commands.lock = function (clientInfo, res, from, names) {
        return modifyLock(clientInfo, repositories.lockAllRepositories, from, "repo.locked", names);
    };

    commands.unlock = function (clientInfo, res, from, names) {
        return modifyLock(clientInfo, repositories.unlockAllRepositories, from, "repo.unlocked", names);
    };

    commands.list = function (clientInfo, res) {
        return repositories.allRepositories(clientInfo).then(function (results) {
            var repos = results.map(function(r){ return r.dataValues; }),
                sorted = _.sortByOrder(repos, ['global','lowertitle'],['desc', 'asc']);
            return render(res, "messages/list", {repositories: sorted});
        }).then(function (message) {
            return hipchat.sendMessage(clientInfo, clientInfo.roomId, message, {"color": "gray"});
        }).catch(function (err) {
            logger.warn(err);
            return err;
        });
    };

    commands.help = function (clientInfo, res) {
        return render(res, "messages/help")
                .then(function (message) {
                    return hipchat.sendMessage(clientInfo, clientInfo.roomId, message, {"color": "gray"});
                }).catch(function (err) {
                    logger.warn(err);
                });
    };

    app.post("/webhook", addon.authenticate(), function (req, res) {
        req.clientInfo.roomId = req.identity.roomId;

        var message = req.body.item.message.message.split(' ');

        message = _.chain(message)
                .map(function(s) {return s.trim()})
                .filter(function(s) { return ! _.isEmpty(s)})
                .value();

        var command = commands.help;
        if (message.length > 1) {
            if (_.contains(["lock", "unlock", "list", "help"], message[1])) {
                command = commands[message[1]]
            }
        }

        command(req.clientInfo, res, req.body.item.message.from, message.slice(2))
                .catch(function(err) {
                    logger.warn(err);
                    return err;
                });

        res.send({});
    }, function (err) {
        console.log(err);
        res.send({});
    });
};