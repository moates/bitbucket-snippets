var cors = require('cors');
var _ = require('lodash');
var logger = require('../lib/logging').module('routes');
var errors = require('../lib/errors');
var bluebird = require('bluebird');

module.exports = function (app, addon) {

    var repositories = addon.repositories;
    var users = require('../model/users')(addon);
    var aliases = require('../model/aliases')(addon);
    var hipchat = require('../lib/hipchat')(addon);
    var title = require('../lib/title')(addon);
    var glances = require('../lib/glances')(addon);    

    var authenticate = function () {
        var restoreUser = function (req, res, next) {
            _.extend(res.locals, req.identity);
            next();
        };

        var handlePromise = function (req, res, next) {
            next().catch(function (ex) {
                logger.warn(ex);
                res.status(500).send({error: "Unexpected error"})
            });
        };

        var authenticate = addon.authenticate();

        return function (req, res, next) {
            authenticate(req, res, function () {
                restoreUser(req, res, function () {
                    handlePromise(req, res, next);
                });
            })
        }
    };

    app.get("/list", authenticate(), function (req, res) {
        var allRepositories = repositories.allRepositories(req.clientInfo);
        var allAliases = aliases.all(req.clientInfo, req.identity.userId);
        
        return bluebird.all([allRepositories, allAliases])
            .spread(function(repositories, aliases) {
                req.session.user = req.query.user;
                var theme = req.query.theme;
                res.expose({
                    repositories: repositories,
                    aliases: aliases,
                    userId: req.session.userId,
                    token: res.locals.signed_request,
                    host: req.headers.host                    
                });
                res.render("main", {theme: theme});
            });
    });

    app.get("/glance", cors(), authenticate(), function (req, res) {
        return glances.renderGlance(req.clientInfo).then(function (data) {
            res.send(data);
        })
    });

    app.post("/lock/:id", authenticate(), function (req, res) {
        var alias = _.get(req.body, 'alias', '@me');
        var getUser;
        
        if ("@me" === alias) {
            getUser = users.findById(req.clientInfo, res.locals.userId)
                .then(function(user) {
                    user.mention_name = '@'+ user.mention_name;
                    return user;
                })
        } else if (_.startsWith(alias,'@')) {
            getUser = Promise.resolve({id: req.identity.userId, mention_name: alias});
        } else {
            getUser = aliases.ensureAlias(req.clientInfo, req.identity.userId, alias)
                .then(_.constant({id: req.identity.userId, mention_name: alias}));
        }
        
        return getUser.then(function (user) {
                    return repositories.lockRepository(req.clientInfo, user, req.params.id)
                })
                .then(function (result) {
                    res.send(result);
                    app.realtime.emit(req.clientInfo, 'repo.locked', [result]);
                    return Promise.all([title.scheduleUpdate(req.clientInfo),
                        glances.updateGlances(req.clientInfo)])
                            .catch(logger.warn);
                });
    });

    app.post("/unlock/:id", authenticate(), function (req, res) {
        return repositories.unlockRepository(req.clientInfo, req.params.id)
                .then(function (result) {
                    res.send(result);
                    app.realtime.emit(req.clientInfo, 'repo.unlocked', [result]);

                    Promise.all([
                        title.scheduleUpdate(req.clientInfo),
                        glances.updateGlances(req.clientInfo)
                    ]).catch(logger.warn);
                });
    });

    app.post("/repos/add", authenticate(), function (req, res) {
        return repositories.addRepository(req.clientInfo, req.body.title, req.body.link, req.body.global)
                .then(function (result) {
                    res.send(result);
                    app.realtime.emit(req.clientInfo, 'repo.added', [result]);
                });
    });

    app.post("/repos/edit", authenticate(), function (req, res) {
        return repositories.updateRepository(req.clientInfo, req.body)
                .then(function (result) {
                    res.send(result);
                    app.realtime.emit(req.clientInfo, 'repo.edited', [result]);
                });
    });

    app.post("/repos/delete", authenticate(), function (req, res) {
        return repositories.deleteRepository(req.clientInfo, req.body.id)
                .then(function (result) {
                    res.send({"result": "success"});
                    app.realtime.emit(req.clientInfo, 'repo.deleted', [result]);
                });
    });
};
