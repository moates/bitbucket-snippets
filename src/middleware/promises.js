module.exports =  function () {

    return function(req, res, next) {
        next().catch(function(ex) {
            logger.warn(ex);
            res.status(500).send({error:"Unexpected error"})
        });
    };

};