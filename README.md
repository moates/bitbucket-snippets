# HipChat Repo Locker Integration

This integration for HipChat provides an interface for locking repositories.

## Getting Started:
1. This application needs a running postgres database (by default it will use the database repo-locker with no authentication).
2. Run npm install
3. Run npm start 

## Instructions for deploying:

1. docker build -t repo-locker-micros .
2. docker tag repo-locker-micros docker.atlassian.io/atlassian/repo-locker-micros:{VERSION}
3. docker push docker.atlassian.io/atlassian/repo-locker-micros:{VERSION}
4. micros service:deploy repo-locker-micros -f repo-locker-micros.sd.yml
