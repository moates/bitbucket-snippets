var module = angular.module('repos', ['ngRoute']);
module.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider
            .when("/list", {
                templateUrl: "partials/list.html",
                controller: "list"
            })
            .when("/add", {
                templateUrl: "partials/add.html",
                controller: "add"
            })
            .when("/edit/:id", {
                templateUrl: "partials/add.html",
                controller: "edit"
            })
            .when("/delete/:id", {
                templateUrl: "partials/delete.html",
                controller: "delete"
            })
            .otherwise({
                redirectTo: "/list"
            });

    $httpProvider.defaults.headers.common['x-acpt'] = window.token;
}]);

module.service('repos', ['$http', '$rootScope', function ($http, $rootScope) {
    var self = this;
    var socket = io(host, {
        transports: ['htmlfile',
            'xhr-polling',
            'jsonp-polling',
            'polling']
    });
    var repositories = window.repositories;
    this.byId = _.object(_.map(repositories, function (r) {return [r.id, r]}));

    this.add = function (title, link, global) {
        return $http.post('/repos/add', {title: title, link: link, global: global})
    };

    this.edit = function (data) {
        return $http.post('/repos/edit', data)
    };

    this.delete = function(id) {
        return $http.post('/repos/delete', {id: id})
    };

    var updateRepository = function (repositories) {
        var update = function () {
            _.each(repositories, function (repository) {
                if (repository && repository.id) {
                    self.byId[repository.id] = repository;
                }
            })
        };

        if ($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest') {
            $rootScope.$apply(update);
        }
        else {
            update();
        }
    };

    this.lockRepository = function (repository, alias) {
        var endpoint = (repository.locked ? "/unlock/" : "/lock/") + repository.id;
        repository.locking = true;
        repository.locked = repository.locked ? false : true;

        return $http.post(endpoint, {alias: alias})
                .success(function (data) {
                    updateRepository([data]);
                    repository.locking = false;
                })
                .error(function (data) {
                    repository.locking = false;
                });
    };

    socket.on('repo.locked', updateRepository);
    socket.on('repo.unlocked', updateRepository);
    socket.on('repo.added', updateRepository);
}]);

module.service('aliases', [function() {
    var self = this;

    this.setDefault = function (alias) {
      this.aliases = [alias].concat(window.aliases);
      this.defaultAlias = alias;
    }

    this.getAliases = function() {
        return this.aliases;
    };

    this.addAlias = function(alias) {
        this.aliases.push(alias);
    }
}]);

module.controller('list', ['repos', '$http', '$interval', '$scope', '$timeout', 'aliases', function (repos, $http, $interval, $scope, $timeout, aliases) {
    $scope.$watch(function () {
        return repos.byId;
    }, function () {
        $scope.repositories = repos.byId;
    }, true);

    $scope.anyGlobalLocked = function () {
        var lockedRepos = _.filter($scope.repositories, function (repository) {return repository.locked; });

        return _.any(lockedRepos, function (repository) {return repository.global; });
    };

    $scope.isRepositoryLocked = function (repository) {
        return $scope.anyGlobalLocked() || repository.locked;
    };

    $scope.lock = function (repository) {
        repos.lockRepository(repository, $scope.alias)
                .error(function (data) {
                    $scope.error = "Oops, there was an error";
                });
    }

    $scope.changeAlias = function() {
        if (! _.contains(aliases.getAliases(), $scope.alias)) {
            aliases.addAlias($scope.alias);
        }
    }

    var query = function query(query) {
        var results = _.map(aliases.getAliases(), function(alias) {return {text: alias, id: alias}});
        return query.callback({results: results});
    }

    AP.require('user', function(user) {
      user.getCurrentUser(function (err, user) {
        $scope.alias = '@' + user.mention_name;

        aliases.setDefault($scope.alias);

          $("#aliases").select2({
            dropdownCssClass: 'aui-select2-drop aui-dropdown2 aui-style-default',
            containerCssClass: 'aui-select2-container',
            query: query,
            createSearchChoice: function(term) {
              return {id: term, text: term, new: true};
            },
            formatResult: function(item) {
              if (item.new) {
                return '<span class="aui-lozenge aui-lozenge-current" style="float:right">New</span>' + item.text;
              } else {
                return item.text;
              }
            },
            placeholder: $scope.alias,
            maximumInputLength: 20
          });

          $("#aliases").change(function(data) {
            $scope.alias = data.val;
            $scope.changeAlias();
          });
      });
    });
}]);

module.controller('add', ['repos', '$location', '$scope', function (repos, $location, $scope) {
    $scope.global = false;

    $scope.save = function () {
        repos.add($scope.title, $scope.link, $scope.global)
                .success(function (data) {
                    repos.byId[data.id] = data;
                    $location.path("/list")
                })
                .error(function (data) {
                    $scope.error = "Oops, there was an error";
                });
    }
}]);

module.controller('edit', ['repos', '$location', '$routeParams', '$scope', function (repos, $location, $routeParams, $scope) {
    var id = $routeParams.id,
        repo = repos.byId[id];

    $scope.global = repo.global;
    $scope.title = repo.title;
    $scope.link = repo.link;

    $scope.save = function () {
        repo.title = $scope.title;
        repo.link = $scope.link;
        repo.global = $scope.global;

        repos.edit(repo)
                .success(function (data) {
                    repos.byId[id] = data[1][0];
                    $location.path("/list")
                })
                .error(function (data) {
                    $scope.error = "Oops, there was an error";
                });
    }
}]);

module.controller('delete', ['repos', '$location', '$routeParams', '$scope', function (repos, $location, $routeParams, $scope) {
    var id = $routeParams.id,
        repo = repos.byId[id];

    $scope.title = repo.title;

    $scope.delete = function () {
        repos.delete(id)
                .success(function (data) {
                    delete repos.byId[id];
                    $location.path("/list")
                })
                .error(function (data) {
                    $scope.error = "Oops, there was an error";
                });
    }
}]);

module.filter('orderRepositories', function() {
    return function(items) {
        return _.sortByOrder(items, ['global','lowertitle'],['desc', 'asc'])
    };
});