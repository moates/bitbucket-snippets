// This is the entry point for your add-on, creating and configuring
// your add-on HTTP server

// Logging
var logger = require('./src/lib/logging');
console.error = logger.error.bind(logger);
console.log = logger.info.bind(logger);
console.warn = logger.warn.bind(logger);

// [Express](http://expressjs.com/) is your friend -- it's the underlying
// web framework that `atlassian-connect-express` uses
var express = require('express');
// You need to load `atlassian-connect-express` to use her godly powers
var ac = require('atlassian-connect-express');
ac.store.register("sequelize", require('./src/model/clients'));
// Static expiry middleware to help serve static resources efficiently
process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(
var expiry = require('static-expiry');
// We use [Handlebars](http://handlebarsjs.com/) as our view engine
// via [express-hbs](https://npmjs.org/package/express-hbs)
var hbs = require('express-hbs');
// We also need a few stock Node modules
var http = require('http');
var path = require('path');
var os = require('os');
var expstate = require('express-state');
var sockets = require('socket.io');
var realtime = require('./src/lib/realtime');
var sessions = require('cookie-session');

// Anything in ./public is served up as static content
var staticDir = path.join(__dirname, 'public');
// Anything in ./views are HBS templates
var viewsDir = __dirname + '/src/views';
// Your routes live here; this is the C in MVC
var routes = require('./src/routes');
// Bootstrap Express
var app = express();
expstate.extend(app);
// Bootstrap the `atlassian-connect-express` library
var addon = ac(app);
// You can set this in `config.json`
var port = addon.config.port();
// Declares the environment to use in `config.json`
var devEnv = app.get('env') == 'development';

var hipchat = require('atlassian-connect-express-hipchat')(addon, app);

// The following settings applies to all environments
app.set('port', port);

// Configure the Handlebars view engine
app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

// Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
app.use(express.favicon());
// Log requests, using an appropriate formatter by env
app.use(express.logger(devEnv ? 'dev' : 'default'));
// Include stock request parsers
app.use(express.bodyParser());
app.use(express.cookieParser());
var session = sessions({secret: addon.config.secret()});
app.use(session);
// Gzip responses when appropriate
app.use(express.compress());
// You need to instantiate the `atlassian-connect-express` middleware in order to get its goodness for free
app.use(addon.middleware());
// Enable static resource fingerprinting for far future expires caching in production
app.use(expiry(app, {dir: staticDir, debug: devEnv}));
// Add an hbs helper to fingerprint static resource urls
hbs.registerHelper('furl', function(url){ return app.locals.furl(url); });
// Mount the add-on's routes
app.use(app.router);
// Mount the static resource dir
app.use(express.static(staticDir));

// Show nicer errors when in dev mode
if (devEnv) app.use(express.errorHandler());

// Wire up your routes using the express and `atlassian-connect-express` objects
addon.repositories = require('./src/model/repositories')(addon);
routes(app, addon);

// Boot the damn thing
var server = http.createServer(app).listen(port, function(){
  console.log('Add-on server running at http://' + os.hostname() + ':' + port);
  // Enables auto registration/de-registration of add-ons into a host in dev mode
  if (devEnv) addon.register();
});

app.io = sockets(server);
app.io.use(function(socket,next) {
  session(socket.request, socket.request.res, next);
});
app.realtime = realtime(app);